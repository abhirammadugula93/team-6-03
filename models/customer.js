/** 
*  Customer model
*  Customer schema to store all customer related details.
*
*  @author Satyavrath Injamuri <s533983@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({

  _id: { 
    type: Number, 
    required: true,
    min:1,
    max:1000000
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  Firstname: {
    type: String,
    required: true,
    default: 'Firstname'
  },
  Lastname: {
    type: String,
    required: true,
    default: 'Lastname'
  },
  street1: {
    type: String,
    required: true,
    default: 'Street 1'
  },
  street2: {
    type: String,
    required: false,
    default: ''
  },
  city: {
    type: String,
    required: true,
    default: 'Maryville'
  },
  state: {
    type: String,
    required: true,
    default: 'MO'
  },
  zip: {
    type: String,
    required: true,
    default: '64468'
  },
  country: {
    type: String,
    required: true,
    default: 'USA'
  }
})
module.exports = mongoose.model('Customer', CustomerSchema)