/**
 *  Order model
 *  Describes the characteristics of each attribute in an order resource.
 *
 * @author Abhiram Madugula <S534093@nwmissouri.edu>
 *
 */

// bring in mongoose
// see <https://mongoosejs.com/> for more information
const mongoose = require("mongoose");
var Schema = new mongoose.Schema();

const OrderSchema = new mongoose.Schema({
  _id: {
    type: Number,
    required: true,
    unique: true
  },
  datePlaced: {
    type: Date,
    required: true,
    default: Date.now()
  },
  dateShipped: {
    type: Date,
    required: false
  },
  paymentType: {
    type: String,
    required: true,
    default: "not selected yet"
  },
  paid: {
    type: Boolean,
    default: false
  },
  customer_id: {
    required: false,
    type: Number
  }
});
module.exports = mongoose.model("Order", OrderSchema);
