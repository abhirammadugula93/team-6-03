#Shop 4 U

#Overview:

Our application is used to build a website for buying products and mainitaining customer information within our database using Model View Controller technique.

#Team Memebers:

Abhiram Madugula - initiator, order

Satyavrath Injamuri - customer

Haritha Atmakuri - product

Susritha Gade - orderline

Bitbucket repo link:
https://bitbucket.org/abhirammadugula93/team-6-03/src/master/


Satyavrath Injamuri - Added Model & Data for Customer
Haritha Atmakuri - Added Model & Data for Product
Abhiram Madugula - Added Model & Data for Order(Added Customer_id in order as order cannot exist without customer). Added orderBoundary in order.js and added logging message in seeer.js
Susritha gade - Added Model and Data for orderlineitems.

Satyavrath Injamuri - Added Controller & Route for Customer, added Views for customer folder (index.ejs, create.ejs, details.ejs, edit.ejs , delete.ejs)
Abhiram Madugula - Added Controller & Route for orders, views folder in which index.ejs, create.ejs, details.ejs, edit.ejs and delete.ejs are added.
Haritha Atmakuri - Added Controller & Route for products, added Views for products folder (index.ejs, create.ejs, details.ejs, edit.ejs , delete.ejs)
Susritha gade - Added Controller & Route for orderline, added Views for orderline folder (index.ejs, create.ejs, details.ejs, edit.ejs , delete.ejs)

Satyavrath Injamuri - Added Controller Action for customer related section, implemented execution of (create,delete,save,details,edit)
Abhiram Madugula -  Added Controller Action for Orders related section, implemented execution of (create,delete,save,details,edit)
Haritha Atmakuri - Added Controller Action for products related section, implemented execution of (create,delete,save,details,edit)
Susritha gade - Added Controller Action for orderline related section, implemented execution of (create,delete,save,details,edit)

Satyavrath Injamuri - Added About me page in our website under "About" tab 
Abhiram Madugula - Added About me page in our website under "About" tab 
Haritha Atmakuri - Added About me page in our website under "About" tab 
Susritha gade - Added About me page in our website under "About" tab 